//
//  Issue.swift
//  TestRxProject
//
//  Created by UDTech LLC on 5/27/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

struct Issue: Codable {
    let identifier: Int
    let number: Int
    let title: String
    let body: String
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case number
        case title
        case body
    }
}
