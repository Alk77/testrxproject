//
//  Repository.swift
//  TestRxProject
//
//  Created by UDTech LLC on 5/27/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

struct Repository: Codable {
    let identifier: Int
    let language: String?
    let url: String
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case language
        case url
        case name
    }
}
