//
//  CustomError.swift
//  Boldcast
//
//  Created by UDTech LLC on 4/25/19.
//  Copyright © 2019 udtech. All rights reserved.
//

import UIKit

enum CustomError: Error {
    case APIError(Int, String?)
    case CoreError(String)
    case NetworkRequestError
    case JsonParsingFailure
    case NoDataError
    case SaveFileError
    case CompileModelError
    case MissingModelError
    case UnderDevelopment
    
    var code: String {
        
        switch self {
        case .APIError(let apiCode, _):
            return " \(String(apiCode))"
        default:
            return ""
        }
    }
    
    var message: String {
        
        switch self {
        case .APIError(_, let str):
            return str ?? "Some non-explained server error."
        case .CoreError(let mess):
            return mess
        case .NetworkRequestError:
            return "No network connection. Please ensure you have internet connection and try again."
        case .JsonParsingFailure:
            return "Cannot to parse server data. Please ensure you have internet connection and try again."
        case .NoDataError:
            return "Cannot receive data from server. Please try to relaunch the application."
        case .SaveFileError:
            return "Cannot to save received data on the device. Please try to relaunch the application."
        case .CompileModelError:
            return "Error by compiling the downloaded machine learning model. Please try to relaunch the application."
        case .MissingModelError:
            return "Error by retrieving the saved machine learning model. Please try to relaunch the application."
        case .UnderDevelopment:
            return "This feature is under development. It will be available soon."
        }
        
    }
}
