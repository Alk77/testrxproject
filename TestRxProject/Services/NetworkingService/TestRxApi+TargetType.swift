//
//  LegoRecognizerApi+TargetType.swift
//  LegoRecognizer
//
//  Created by UDTech LLC on 5/7/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import Moya

extension TestRxApi: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }
    
    var path: String {
        switch self {
        case .repos(let name):
            return "/users/\(name.URLEscapedString)/repos"
        case .userProfile(let name):
            return "/users/\(name.URLEscapedString)"
        case .repo(let name):
            return "/repos/\(name)"
        case .issues(let repositoryName):
            return "/repos/\(repositoryName)/issues"
        }
    }

    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        switch self {
        case .repos(_):
            return """
            [
            {"id": "1",
            "language": "Swift",
            "url": "https://api.github.com/repos/mjacko/Router",
            "name": "Router"}
            ]
""".utf8Encoded
        case .userProfile(let name):
            return "{\"login\": \"\(name)\", \"id\": 100}".utf8Encoded
        case .repo(_):
            return """
            {"id": "1",
            "language": "Swift",
            "url": "https://api.github.com/repos/mjacko/Router",
            "name": "Router"}
""".utf8Encoded
        case .issues(_):
            return """
            {"id": 132942471,
            "number": 405,
            "title": "Updates example with fix to String extension by changing to Optional",
            "body": "Fix it pls."}
""".utf8Encoded
        }
    }
    
    var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return ["Content-type": "application/json", "Accept": "application/json"]
        }
    }
}

private extension String {
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
    
    var URLEscapedString: String {
        return addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}
