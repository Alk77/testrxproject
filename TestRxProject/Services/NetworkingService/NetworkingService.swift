//
//  File.swift
//  LegoRecognizer
//
//  Created by UDTech LLC on 5/7/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import Moya
import Result
import RxSwift

protocol INetworkingService {
    func repos(name: String) -> Single<[Repository]>
}

class NetworkingService: INetworkingService {
    
    let testRxApi: MoyaProvider<TestRxApi>

    init(endpointClosure: @escaping MoyaProvider<TestRxApi>.EndpointClosure = MoyaProvider<TestRxApi>.defaultEndpointMapping,
         requestClosure: @escaping MoyaProvider<TestRxApi>.RequestClosure = MoyaProvider<TestRxApi>.defaultRequestMapping,
         stubClosure: @escaping MoyaProvider<TestRxApi>.StubClosure = MoyaProvider.neverStub,
         manager: Manager = MoyaProvider<TestRxApi>.defaultAlamofireManager(),
         plugins: [PluginType] = [],
         trackInflights: Bool = false) {
        
        self.testRxApi = MoyaProvider(endpointClosure: endpointClosure, requestClosure: requestClosure, stubClosure: stubClosure, manager: manager, plugins: plugins, trackInflights: trackInflights)
    }
//    var testRxClosure = { (target: TestRxApi) -> Endpoint in
//
//        let url = target.baseURL.absoluteString + target.path
//
//        let endpoint = Endpoint(
//            url: url,
//            sampleResponseClosure: { .networkResponse(200, target.sampleData) },
//            method: target.method,
//            task: target.task,
//            httpHeaderFields: target.headers
//        )
//
//        print("Request url:\(url)")
//        return endpoint.adding(newHTTPHeaderFields: [:])
//    }
//
//    let jsonDecoder = JSONDecoder()
    
//    init() {
////        self.testRxApi = MoyaProvider<TestRxApi>(endpointClosure: testRxClosure)
//        self.testRxApi = MoyaProvider<TestRxApi>(stubClosure: MoyaProvider<TestRxApi>.immediatelyStub) // used .sampleData
//    }
    
    func repos(name: String) -> Single<[Repository]> {
        let result = testRxApi.rx.request(.repos(username: name))
            .debug()
            .map([Repository].self)
        return result
    }
    
    func checkDBVersion(_ current: String, completion: @escaping (String?, CustomError?) -> Void) {
//        testRxApi.request(.checkDBVersion(currentVersion: current)) { (result) in
//            switch result {
//            case .success(let response):
//                guard let objectResponse = try? response.map(Version.self, using: self.jsonDecoder) else {
//                    completion(nil, CustomError.JsonParsingFailure)
//                    return
//                }
//                completion(objectResponse.version, nil)
//
//            case .failure(let error):
//                completion(nil, CustomError.APIError(error.errorCode, error.errorDescription))
//            }
//        }
    }
    
    func updateDB(completion: @escaping (Data?, CustomError?) -> Void) {
//        testRxApi.request(.updateDB) { (result) in
//            switch result {
//            case .success(let response):
//                completion(response.data, nil)
//
//            case .failure(let error):
//                completion(nil, CustomError.APIError(error.errorCode, error.errorDescription))
//            }
//        }
    }
    
    func updateModel(progressCompletion: @escaping(Double) -> Void, completion: @escaping (Data?, CustomError?) -> Void) {
//        testRxApi.request(.updateModel, callbackQueue: DispatchQueue.main, progress: { (progress) in
//            progressCompletion(progress.progress)
//        }) { (result) in
//            switch result {
//            case .success(let response):
//                completion(response.data, nil)
//
//            case .failure(let error):
//                completion(nil, CustomError.APIError(error.errorCode, error.errorDescription))
//            }
//        }
    }
    
//    fileprivate func request(_ token: TestRxApi) -> Observable<Moya.Response> {
//        let actualRequest = testRxApi.rx.request(token)
//        return online
//            .ignore(value: false)  // Wait until we're online
//            .take(1)        // Take 1 to make sure we only invoke the API once.
//            .flatMap { _ in // Turn the online state into a network request
//                return actualRequest
//                    .filterSuccessfulStatusCodes()
//                    .do(onSuccess: { (response) in
//                    }, onError: { (error) in
//                        if let error = error as? MoyaError {
//                            switch error {
//                            case .statusCode(let response):
//                                if response.statusCode == 401 {
//                                    // Unauthorized
//                                    //                                    AuthManager.removeToken()
//                                }
//                            default: break
//                            }
//                        }
//                    })
//        }
//    }
}
