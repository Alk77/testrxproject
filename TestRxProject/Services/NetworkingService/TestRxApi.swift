//
//  LegoRecognizerApi.swift
//  LegoRecognizer
//
//  Created by UDTech LLC on 5/7/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import Foundation

enum TestRxApi {
    case userProfile(username: String)
    case repos(username: String)
    case repo(fullName: String)
    case issues(repositoryFullName: String)
}
