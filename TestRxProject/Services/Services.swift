//
//  Services.swift
//  TestRxProject
//
//  Created by UDTech LLC on 5/27/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

class Services {

    static let shared = Services()
    
    // MARK: - private services
    
    lazy var networking = NetworkingService.init()
    
    // MARK: - public services
    
    
}
