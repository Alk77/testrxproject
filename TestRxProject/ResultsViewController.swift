//
//  ResultsViewController.swift
//  TestRxProject
//
//  Created by UDTech LLC on 4/17/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ResultsViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    var repositoryName: Observable<String> {
        return searchBar.rx.text
            .orEmpty
            .do(onNext: { print("Entered:", $0) }, onError: { print("Intercepted error:", $0) }, onCompleted: { print("Completed")  })
            .filter {  $0.count > 2 }
            .throttle(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
    }
    var resultsViewModel: ResultsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ResultsTableViewCell.self, forCellReuseIdentifier: "ResultsTableViewCell")
        tableView.tableFooterView = UIView(frame: .zero)
        bindViewModel()

        // Do any additional setup after loading the view.
    }
    
    func bindViewModel() {
        resultsViewModel = ResultsViewModel(repositoryName: repositoryName)
        resultsViewModel.findRepositories()
            .bind(to: tableView.rx.items(cellIdentifier: "ResultsTableViewCell",
                                         cellType: ResultsTableViewCell.self)) {  row, element, cell in
                                            cell.titleLabel?.text = "\(row) \(element.name ?? "") \(element.language ?? "")"
                                            cell.descriptionLabel?.text = "\(element.name ?? "") \(element.language ?? "")"
            }
            .disposed(by: disposeBag)
    }
}
