//
//  StartViewController.swift
//  TestRxProject
//
//  Created by UDTech LLC on 4/16/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StartViewController: UIViewController {
    
    
    @IBOutlet weak var goButton: UIButton!
    
    let disposeBag = DisposeBag()
    let toResultsSegue = "fromStartToResultsSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        goButton.rx.tap
            .bind {
                [weak self] _ in self?.goPressed()
            }
            .disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let radius: CGFloat = UIDevice.current.userInterfaceIdiom == .phone ? 6.0 : 10.0
        self.goButton.layer.cornerRadius = radius
        
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: self.goButton.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor = UIColor.clear.cgColor
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 3.5)
        shadowLayer.shadowOpacity = 0.25
        shadowLayer.shadowRadius = 3.5
        
        self.goButton.layer.insertSublayer(shadowLayer, at: 0)
        
        let colorBottom = UIColor(red: 0.0 / 255.0, green: 145.0 / 255.0, blue: 147.0 / 255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 115.0 / 255.0, green: 252.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0).cgColor
        
        let gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        gl.frame = self.view.frame
        self.view.layer.insertSublayer(gl, at: 0)
    }
    
    func setupBinding() {
        goButton.rx.tap
            .bind {
                [weak self] _ in self?.goPressed()
            }
            .disposed(by: disposeBag)
    }
    
    func goPressed() {
        self.performSegue(withIdentifier: toResultsSegue, sender: self)
    }
    
}
