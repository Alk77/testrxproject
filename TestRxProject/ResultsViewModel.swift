//
//  ResultsViewModel.swift
//  TestRxProject
//
//  Created by UDTech LLC on 5/27/19.
//  Copyright © 2019 UDTech LLC. All rights reserved.
//

import Foundation
import Moya
import RxOptional
import RxSwift

protocol IResultsViewModel {
//    func trackIssues() -> Observable<[Issue]>
    func findRepositories() -> Observable<[Repository]>
}

struct ResultsViewModel: IResultsViewModel {
    var repositoryName: Observable<String>
//
//    func trackIssues() -> Observable<[Issue]> {
//        return repositoryName
//        .observeOn(MainScheduler.instance)
//            .flatMapLatest { name -> Observable<Repository?> in
//            return self.findRepository(name)
//        }
//            .flatMapLatest { repository -> Observable<[Issue]?> in
//                guard let repository = repository else { return Observable.just(nil) }
//                return self.findIssues(repository: repository)
//        }
//            .replaceNilWith([])
//    }
//
//    private func findIssues(repository: Repository) -> Observable<[Issue]?> {
//        return Observable.just(nil)
//    }
//
//    private func findRepository(_ repositoryName: String) -> Observable<Repository?> {
//        return Observable.just(nil)
//    }
    
    func findRepositories() -> Observable<[Repository]> {
        return repositoryName.observeOn(MainScheduler.instance)
            .flatMapLatest({ name -> Observable<[Repository]> in
                return Services.shared.networking.repos(name: name).asObservable()
            })
//        return Services.shared.networking.repos(name: repositoryName)
//        .observeOn(MainScheduler.instance)
//            .flatMapLatest({ (repositories) -> Observable<[Repository]> in
//                print(repositories)
//                return Observable.just(repositories)
//            })
    }
}
